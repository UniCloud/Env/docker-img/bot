FROM singularity

ENV DEBIAN_FRONTEND noninteractive

RUN apt install -y lxterminal pcmanfm git ansible
RUN apt install -y chromium-browser

# as user
USER me
WORKDIR /home/me
ADD .ssh /home/me/.ssh

USER root
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
